$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ){
        var min = parseInt( minVal, 10 );
        var max = parseInt( maxVal, 10 );
        var age = parseFloat( data[3] ) || 0; // use data for the age column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age   && isNaN( max ) ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
);



var table, maxVal, minVal;
 
$(document).ready(function(){

	
	
	$.getJSON("https://api.myjson.com/bins/14qdrz", function(result){
		$(".loadingData").remove();
		$.each(result, function(i, field){
			ac=i+1;
			$("#jData").append('<tr id="dataRow_'+parseInt(ac)+'"><td id="colID_'+ac+'"><a data-toggle="modal" data-target="#modal-fullscreen" onclick="editAction('+parseInt(ac)+')" href="javascript:void(0);" id="editAction_'+parseInt(ac)+'">'+field.id+'</a></td><td id="colFName_'+ac+'">'+field.fname+'</td><td id="colLName_'+ac+'">'+field.lname+'</td><style>#colSalary_'+ac+':before{content:"'+field.salary+'"}</style><td id="colSalary_'+ac+'">'+parseInt((field.salary).replace("$", "").replace(",","").replace(",",""))+'</td><td id="colPoints_'+ac+'">'+field.points+'</td><td id="colRebounds_'+ac+'">'+field.rebounds+'</td><td id="colAssists_'+ac+'">'+field.assists+'</td><td id="colSteals_'+ac+'">'+field.steals+'</td><td id="colBlocks_'+ac+'">'+field.blocks+'</td><td><a class="delThisRow" id="delAction_'+parseInt(ac)+'" href="javascript:void(0);">Delete</a></td></tr>');
		});
		
		table = $('#pList').DataTable({
			"paging":false
		});
	});



	$( "#slider-range" ).slider({
		range: true,
		min: 0,
		max: 10000000,
		values: [1000000,8000000],
		slide: function(event, ui){
			minVal = ui.values[0];
			maxVal = ui.values[1];
			$("#amount").val("$" + ui.values[0] + " - $" + ui.values[ 1 ] );
			table.draw();
		}
	});
	
	
	
	
	
		$("#saveAction").click(function(){
			
			jQuery.validator.addMethod("lettersonly", function(value, element) {
			  return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Letters only please");
			
			$("#actionForm").validate({
				rules: {
					u_fName:{required:true,lettersonly:true},
					u_lName:{required:true,lettersonly:true},
					u_salary:{required:true},
					u_points:{required:true,number:true},
					u_Bounds:{required:true,number:true},
					u_assists:{required:true,number:true},
					u_steals:{required:true,number:true},
					u_blocks:{required:true,number:true}
				}
			});
			
			
			if($("#actionForm").valid()){
				if($("#actionForm").hasClass("frm_editMode")){
					var modRowId = $("#actionForm").attr("data-getid");
					$("#colFName_"+modRowId).text($("#u_fName").val());
					$("#colLName_"+modRowId).text($("#u_lName").val());
					$("#colSalary_"+modRowId).text($("#u_salary").val());
					$("#colPoints_"+modRowId).text($("#u_points").val());
					$("#colRebounds_"+modRowId).text($("#u_Bounds").val());
					$("#colAssists_"+modRowId).text($("#u_assists").val());
					$("#colSteals_"+modRowId).text($("#u_steals").val());
					$("#colBlocks_"+modRowId).text($("#u_blocks").val());
				}
				
				if($("#actionForm").hasClass("frm_newEntry")){
					var nrId = (($("#jData tr").length)+1);
					var jRow = $('<tr id="dataRow_'+nrId+'" role="row" class="odd">')
					.append('<td id="colID_'+nrId+'"><a data-toggle="modal" data-target="#modal-fullscreen" onclick="editAction('+nrId+')" href="javascript:void(0);" id="editAction_'+nrId+'">'+nrId+'</a></td>'+
							'<td id="colFName_'+nrId+'">'+$("#u_fName").val()+'</td>'+
							'<td id="colLName_'+nrId+'">'+$("#u_lName").val()+'</td>'+
							'<style>#colSalary_'+nrId+':before{content:"'+$("#u_salary").val()+'"}</style><td id="colSalary_'+nrId+'">'+$("#u_salary").val()+'</td>'+
							'<td id="colPoints_'+nrId+'">'+$("#u_points").val()+'</td>'+
							'<td id="colRebounds_'+nrId+'">'+$("#u_Bounds").val()+'</td>'+
							'<td id="colAssists_'+nrId+'">'+$("#u_assists").val()+'</td>'+
							'<td id="colSteals_'+nrId+'">'+$("#u_steals").val()+'</td>'+
							'<td id="colBlocks_'+nrId+'">'+$("#u_blocks").val()+'</td>'+
							'<td><a class="delThisRow" id="delAction_'+nrId+'" href="javascript:void(0);">Delete</a></td>');
					table.row.add(jRow).draw();
				}
				
				table.draw();
				$("#cancelAction").trigger("click");
			}
			
		});
    
    
	$('#pList').on( 'click', '.delThisRow', function(){
		table.row($(this).parents('tr')).remove().draw();
	});
	
	
	$('#toggle-event').change(function(){
		if($(this).prop('checked')){
			$('#filterAD').val("Descending");
			$("#filterBy, #filterAD").removeAttr("disabled");
		}
		else{
			$("#filterBy, #filterAD").attr("disabled","disabled");
		}
	});
	
	
	
	$(".modal-fullscreen").on('show.bs.modal', function(){
		setTimeout( function() {
		$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
		}, 0);
		});
		$(".modal-fullscreen").on('hidden.bs.modal', function () {
		$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	});
	
	
	
	$("#pList thead tr th").each(function(i){
		$('#filterBy').append($('<option>',{
		value:"sortById_"+parseInt(i+1),
		text: $(this).text(),
		class:'pl_'+parseInt(i+1)
		}));
	});
	
	$("#filterBy option[value='sortById_1'], #filterBy option[value='sortById_2'], #filterBy option[value='sortById_3'], #filterBy option[value='sortById_10']").remove();
	
	
	$("#filterBy").change(function(){
		$('#filterAD').val("Descending");
	});
	
	$("#filterAD").change(function(){
		//alert($('#filterBy:selected').attr('class'));
		
		var sortClick = $('select[id="filterBy"] :selected').attr('class');
		$("#pList ."+sortClick).trigger("click");
	});

    
});





function editAction(editId){
	$(".modal-title").html("Edit Player");
	
	$("#actionForm").attr({"class":"","data-getId":editId}).addClass("frm_editMode");
	
	$("#u_fName").val($("#colFName_"+editId).text());
	$("#u_lName").val($("#colLName_"+editId).text());
	$("#u_salary").val($("#colSalary_"+editId).text());
	$("#u_points").val($("#colPoints_"+editId).text());
	$("#u_Bounds").val($("#colRebounds_"+editId).text());
	$("#u_assists").val($("#colAssists_"+editId).text());
	$("#u_steals").val($("#colSteals_"+editId).text());
	$("#u_blocks").val($("#colBlocks_"+editId).text());
}

function addNewEntry(){
	$(".modal-title").html("Add New Player");
	$("#actionForm").find("input").val("");
	$("#actionForm").attr({"class":""}).addClass("frm_newEntry");
}